﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float velocidadY = 5;
    private float posY;
    private float direction;
    private Vector3 newPosition = new Vector3(0,0,0);
    public float maxY = 3.8f;
    public float minY = -3.8f;

    // Update is called once per frame
    void Update()
    {
        //movimiento vertical de la pala
        direction = Input.GetAxis("Vertical");

        //printar posicion pala
        Debug.Log(transform.position.y);
        Debug.Log(transform.rotation.y);

        posY = transform.position.y + direction * velocidadY * Time.deltaTime;

        newPosition.x = transform.position.x;
        newPosition.y = posY;
        newPosition.z = transform.position.z;
        transform.position = newPosition;

        if (posY > maxY)//limites player
        {
            Debug.Log("MAX Y");
            newPosition.y = maxY;
            transform.position = newPosition;
        }if (posY < minY) {
            Debug.Log("MIN Y");
            newPosition.y = minY;
            transform.position = newPosition;
        }
    }
}
